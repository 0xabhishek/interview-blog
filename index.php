<?php

	// include_once('asdasd');exit;

	$__CONFIG = json_decode(file_get_contents(".bullwhip.json"),true);

	define("__ROOT_DIR",__DIR__);

	$__APP_CONFIG = $__CONFIG["APP_CONFIG"];
	$__API_CONFIG = $__CONFIG["API_CONFIG"];
	$__CONSTANTS = $__APP_CONFIG["CONSTANTS"];
	$__URL_MAPPER = $__API_CONFIG["URL_MAPPER"];
	$__REQUIRE_DIRS = $__API_CONFIG["REQUIRE_DIRS"];

	ini_set('memory_limit',$__CONSTANTS["APP_MEM_LIMIT"]);

	header('Access-Control-Allow-Origin: *');

	$contentType = isset($_REQUEST["encoding"]) && strtolower($_REQUEST["encoding"]) === "xml" ? "text/xml" : "application/json";
	// header('Content-Type: ' . $contentType);

	require_once("utilities/helpers/Helper.class.php");
	require_once("controllers/BaseController.class.php");
	
	foreach($__REQUIRE_DIRS as $dir) {
		Helper::requireFiles($dir);
	}

	if(!isset($_REQUEST["url"]) || empty($_REQUEST["url"])) {
		echo ResponseManager::getResponse(array("status"=>array("message"=>"endpoint not found","code"=>404,"setHTTPStatus"=>true),$_REQUEST));
		exit;
	}


	preg_match($__CONSTANTS["ENDPOINT_URL_REGEX"],$_REQUEST["url"],$matches);
	
	$_REQUEST["url"] = $matches[$__CONSTANTS["MATCH_GROUP"]];

	$controllerName = isset($__URL_MAPPER[$_REQUEST["url"]]) ? $__URL_MAPPER[$_REQUEST["url"]] : null;

	if($controllerName === null) {
		echo ResponseManager::getResponse(array("status"=>array("message"=>"{$_REQUEST['url']}: This url does not exist","code"=>404,"setHTTPStatus"=>true),$_REQUEST));
		exit;
	}

	$controller = new $controllerName();
	if(!method_exists($controller, 'isExtended')) {
		echo ResponseManager::getResponse(array("status"=>array("message"=>"$controllerName: This class is not extended from base controller","code"=>500),$_REQUEST));
		exit;
	}
	// print_r($_REQUEST);exit;

	/* Parse incoming data and add it to $_REQUEST */


	if(strtolower($_SERVER['REQUEST_METHOD']) === 'post' || strtolower($_SERVER['REQUEST_METHOD']) === 'put' || strtolower($_SERVER['REQUEST_METHOD']) === 'delete') {
		$requestBody = file_get_contents("php://input");

		if(!empty($requestBody)) {

			$regex = "/^.*FormBoundary.*\n?Content-Disposition: form-data; name=\"(.*)\"[.\n]?([^-]+)/";
			
			if(preg_match_all($regex, $requestBody)) {
				//content body format: WebKitFormBoundary , multipart/form-data
				while(preg_match_all($regex, $requestBody, $matches)) {
					//trim hack with regex
					$_REQUEST[$matches[1][0]] = preg_replace("/^(\s*)?(\n*)?/",'',$matches[2][0]);
					$_REQUEST[$matches[1][0]] = preg_replace("/(\s*)?(\n*)?$/",'',$_REQUEST[$matches[1][0]]);
					$requestBody = preg_replace($regex, '', $requestBody);
				}
			} elseif ($rbJson = json_decode($requestBody,true)) {
				//content body format: JSON
				$_REQUEST = array_merge($rbJson,$_REQUEST);
			} else {
				//content body format: KeyValue Pairs
				parse_str($requestBody,$kvpData);
				//malformed case is handled in the controller. Done to save time
				$_REQUEST = array_merge($kvpData,$_REQUEST);
			}
		}
	}


	$_REQUEST["verb"] = strtolower($_SERVER['REQUEST_METHOD']);
	ob_start('ob_gzhandler');
	echo $controller->developView($_REQUEST); // hack for now ; will change later [json_decode part]

	
	// echo json_encode($response);

?>
