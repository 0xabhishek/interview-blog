<?php

	class GoogleSuggestAPI {
		
		private static $url = "http://suggestqueries.google.com/complete/search?output=firefox&ds=yt&q=";

		public static function getResponse($query) {
			return json_decode(file_get_contents(self::$url . urlencode($query)),true);
		}

	}

?>