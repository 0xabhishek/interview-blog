Accessing the API:
==================

Assuming the directory is placed on the document root of the webserver:


GET all blogs paginated with a soft limit of 5 and a hard limit of 20:

	GET http://localhost/interview-blog/posts

	Params: 
		postId : id of the post to get a particular blog post
		limit : number of blog posts to fetch in one request when no postId is present (default is 5 - max is 20)
		page : page number for pagination (default is 1)

	Sorting:
		Sorted by default with creation date in descending order (latest first)

	Eg: 
		http://localhost/interview-blog/posts?limit=15&page=2
		http://localhost/interview-blog/posts?postId=12    (This will fetch a particular blog post with the postId 12 if exists)


POST or CREATE a blog:

	POST http://localhost/interview-blog/posts
		
		Request Data: **Please note, data cannot be passed as a JSON string
			title="<blog post title>"&
			content="<blog content - paragraphs can be separated using 2 newline characters>"

		Result:
			This returns the postId of the blog on success or err msg on failure with the relevant status code

POST or CREATE a comment:

	POST http://localhost/interview-blog/posts

		Request Data:
			__action=comment&
			comment=<comment string here>&
			paraId=<paragraphId which is present on the GET request of the post>&
			postId=<id of the post which the para belongs to>

		**Please note, instead of having multiple level URIs, I chose to build this framework to have something called __action

DELETE a blog:

	DELETE http://localhost/interview-blog/posts
		
		Request Data: **Please note, data cannot be passed as a JSON string
			postId="<id of post>"

		Result:
			This returns the postId of the blog on success or err msg on failure with the relevant status code

DELETE a comment:

	DELETE http://localhost/interview-blog/posts
		
		Request Data: **Please note, data cannot be passed as a JSON string
			__action=comment&
			commentId="<id of comment>"

		Result:
			This returns the postId of the blog on success or err msg on failure with the relevant status code