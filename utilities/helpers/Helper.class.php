<?php

	class Helper {

		public static function timetostr($d,$onlyFirst=true) {
			// date_default_timezone_set('Asia/Kolkata');
			$t1 = date_create("now");
			$t2 = date_create($d);
			
			$interval = date_diff($t2,$t1);
			$whenTime = "";
			$string = "";
			$arr = array();
			
			if($interval->y !== 0) {
				$arr[] = $interval->y == 1 ? $interval->y." year " : $interval->y." years ";
			}
			if($interval->m !== 0) {
				$arr[] = $interval->m == 1 ? $interval->m." month " : $interval->m." months ";
			}
			if($interval->d !== 0) {
				$arr[] = $interval->d == 1 ? $interval->d." day " : $interval->d." days ";
			}
			if($interval->h !== 0) {
				$arr[] = $interval->h == 1 ? $interval->h." hour " : $interval->h." hours ";
			}
			if($interval->i !== 0) {
				$arr[] = $interval->i == 1 ? $interval->i." min " : $interval->i." mins ";
			}
			if($interval->s !== 0) {
				$arr[] = $interval->s == 1 ? $interval->s." second " : $interval->s." seconds ";
			}

			if($onlyFirst) {
				$string = isset($arr[0]) && !empty($arr[0]) ? $arr[0] : "few secs ";
			} else {
				$string = join(" ",$arr);
			}
			
			switch($interval->format("%R")) {
				case "+": {
					$whenTime = " ago";
					$timetostr = rtrim($string, " ").$whenTime;
					break;
				}
				case "-": {
					#$whenTime = " later";
					$timetostr = rtrim($string, " ").$whenTime;
					break;
				}
				default : {
					$whenTime = "right now";
					$timetostr = rtrim($string, " ").$whenTime;
				}
			}
			return $timetostr;
		}

		public static function microtime() {
		    list($usec, $sec) = explode(" ", microtime());
		    return ((float)$usec + (float)$sec);
		}

		public static function requireFiles($dir, $parentPath=".", $exclude=array('Helper.class.php'=>1,'PDO_Errors.log'=>1)) {
			
			if(!is_dir("$parentPath/$dir")) return;
			
			if($fd = opendir("$parentPath/$dir")) {
				while(FALSE !== ($file = readdir($fd))) {
					if($file === '.' || $file === '..') {
						continue;
					}
					if(is_dir("$parentPath/$dir/$file")) {
						self::requireFiles("$file","$parentPath/$dir");
					} else if(!isset($exclude["$file"])){
						include_once("$parentPath/$dir/$file");
						// echo "including: $parentPath/$dir/$file\n";
					}
				}
			}
		}

	}

?>