<?php

	class ResponseManager {

		public static function getResponse($config) {
			$toReturn = array();

			if(isset($config['status'])) {
				
				if(isset($config['status']['setHTTPStatus']) && $config['status']['setHTTPStatus']) {
					header(':', true, isset($config['status']['code']) ? $config['status']['code'] : 200);
				}
				$toReturn['statusCode'] = isset($config['status']['code']) ? $config['status']['code'] : 200;
				$toReturn['success'] = ($toReturn['statusCode'] >= 200 && $toReturn['statusCode'] < 400) ? true : false;
				if(isset($config['status']['message'])) {
					$toReturn['message'] = $config['status']['message'];
				}
			}

			if(isset($config['rootParams']) && is_array($config['rootParams'])) {
				$toReturn = array_merge($toReturn, $config['rootParams']);
			}

			if(isset($config['params'])) {
				unset($config['params']['url']);
				$toReturn['requestParams'] = $config['params'];
			}

			if(isset($config['warnings'])) {
				$toReturn['warnings'] = $config['warnings'];
			}

			$toReturn['dataCount'] = 0;
			$toReturn['data'] = array();
			
			if(isset($config['data'])) {
				$toReturn['dataCount'] = count($config['data']);
				$toReturn['data'] = $config['data'];
			}

			// if(!isset($config["params"]["encoding"]) || !strlen($config["params"]["encoding"])) {
			// 	$config["params"]['encoding'] = "json";
			// }
			$config["params"]["encoding"] = "json";

			// echo $config["params"]["encoding"];exit;

			switch(strtolower($config["params"]["encoding"])) {

				case "xml": {
					//incomplete
					break;
				}

				case "json": {
					$toReturn = json_encode($toReturn);
					break;
				}

				default: {
					$toReturn = json_encode($toReturn);
					break;
				}
			}


			return $toReturn;
		}

	}

?>