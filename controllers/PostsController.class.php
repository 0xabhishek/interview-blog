<?php

	class PostsController extends BaseController{

		private $actions = array(
			"comment"=>array("post"=>"addComment", "delete"=>"deleteComment")
		);

		public function developView($params) {
			
			$verb = $params["verb"];
			unset($params["verb"]);

			switch($verb) {
				case "get": {
					
					// Write your query with Joins until WHERE clause
					$query = "SELECT p.post_id postId, title, GROUP_CONCAT(para_id) paraIds, GROUP_CONCAT(para) paras FROM posts p LEFT OUTER JOIN content con ON p.post_id=con.post_id WHERE ";

					//these two variables work like 2 buckets, each holding a query condition and its value to be bound
					$qParams = array();
					$queryArr = array();

					$postIdExists = false;

					$db = new PDOWrapper();
					// for params set in GET Request, check them here and add them to queryArr and qParams as shown below
					
					// limit by postId
					if(isset($params["postId"]) && strlen($params["postId"])) {
						$queryArr[] = "p.post_id=?";
						$qParams[] = $params["postId"];
						$postIdExists = true;
					}

					// if(isset($params["para"]) && strlen($params["para"])) {
					// 	$queryArr[] = "con.para like ?";
					// 	$qParams[] = "%" . $params["para"] . "%";
					// }

					//NOTE: you can copy paste the above code to add more params to allow API control from GET Request

					// use this to set limit for pagination: 25 is the default max limit here. you can change it
					$hardLimit = 20;
					$limit = 5;
					if(isset($params["limit"]) && strlen($params["limit"])) {
						$limit = intval($params["limit"]);
						$limit = $limit <= $hardLimit ? $limit : $hardLimit;
					}

					$limitStart = 0;
					if(isset($params["page"]) && strlen($params["page"]) && $params["page"] > 0) {
						$page = intval($params["page"]) - 1;

						$limitStart = $page * $limit;
					}

					// this creates the query params and joins them to the main query
					$query .= count($queryArr) > 0 ? implode(" AND ",$queryArr) : 1;

					// order and limit your query if needed
					$query .= " GROUP BY p.post_id ORDER BY created_at DESC LIMIT $limitStart,$limit";

					// uncomment the line immediately below this comment to verify your query
					// echo $query;print_r($qParams);exit;

					//run your query here

					$data = $db->pdoQuery($query,$qParams)->results();

					if($postIdExists) {
						$comments = array();
						$commentQuery = "SELECT comment_id commentId, comment, para_id paraId, created_at createdAt FROM comments WHERE post_id=?";

						$commentsTemp = $db->pdoQuery($commentQuery, array($params["postId"]))->results();
						foreach($commentsTemp as $commentTemp) {
							// print_r($commentTemp);
							$paraIdTemp = $commentTemp["paraId"];
							unset($commentTemp["paraId"]);
							$comments[$paraIdTemp][] = $commentTemp;
						}
					}


					foreach($data as $k => $datum) {
						$parasTemp = explode(",",$datum["paras"]);
						unset($data[$k]["paras"]);
						$paraIdsTemp = explode(",",$datum["paraIds"]);
						unset($data[$k]["paraIds"]);
						$data[$k]["content"] = array();
						foreach($parasTemp as $l => $p) {
							$data[$k]["content"][$l] = array("paraId"=>$paraIdsTemp[$l],"para"=>$p);
							if($postIdExists) {
								$data[$k]["content"][$l]["comments"] = $comments[$paraIdsTemp[$l]];
							}
						}
					}

					// print_r($data);exit;

					// use the response manager class to define your final response. You could only set "data" too. And even data is optional but that would be pointless, would'nt it?
					return ResponseManager::getResponse(array("data"=>$data,"status"=>array("message"=>"Posts data fetched"),"rootParams"=>array("page"=>1),"params"=>$params));
				}
				
				case "post": {
					if(isset($params["__action"]) && strlen($params["__action"]) && isset($this->actions[$params["__action"]][$verb])) {
						$action = $this->actions[$params["__action"]][$verb];
						return $this->$action($params);
					}

					if(!isset($params["title"]) || !strlen($params["title"])) {
						return ResponseManager::getResponse(array("status"=>array("message"=>"title needs to be present","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
					}

					if(!isset($params["content"]) || !strlen($params["content"])) {
						return ResponseManager::getResponse(array("status"=>array("message"=>"content needs to be present","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
					}

					$paras = preg_split("/\\r\\n\\r\\n|\\r\\r|\\n\\n/", $params["content"]);


					$db = new PDOWrapper();

					$postCheck = "SELECT post_id FROM posts WHERE title=?";
					if($db->pdoQuery($postCheck, array($params["title"]))->affectedRows() > 0) {
						return ResponseManager::getResponse(array("status"=>array("message"=>"cannot post blog with same title","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
					}
					
					$postInsert = "INSERT INTO posts (title) VALUES (?)";

					$postId = $db->pdoQuery($postInsert, array($params["title"]))->getLastInsertId();

					// print_r($paras);exit;

					$paraInsert = "INSERT INTO content (para, post_id) VALUES (?,?)";
					foreach($paras as $para) {
						$db->pdoQuery($paraInsert, array($para, $postId));
					}
					
					return ResponseManager::getResponse(array("data"=>array("postId"=>$postId),"status"=>array("message"=>"post inserted"),"rootParams"=>array("page"=>1),"params"=>$params));
				}

				case "delete": {
					if(isset($params["__action"]) && strlen($params["__action"]) && isset($this->actions[$params["__action"]][$verb])) {
						$action = $this->actions[$params["__action"]][$verb];
						return $this->$action($params);
					}

					//delete blog if id is present
					if(!isset($params["postId"]) || !strlen($params["postId"])) {
						return ResponseManager::getResponse(array("status"=>array("message"=>"postId needs to be present","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
					}

					$db = new PDOWrapper();

					$deletePost = "DELETE FROM posts WHERE post_id=?";
					$isDeleted = $db->pdoQuery($deletePost, array($params["postId"]))->affectedRows() > 0 ? true : false;

					if(!$isDeleted) {
						return ResponseManager::getResponse(array("status"=>array("message"=>"postId invalid","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
					}

					return ResponseManager::getResponse(array("data"=>array("postId"=>$postId),"status"=>array("message"=>"post deleted"),"rootParams"=>array("page"=>1),"params"=>$params));

				}
				
				// you could handle the above cases using the default itself if youre sure the post put and delete is not required. Both ways are ok
				default: {
					return ResponseManager::getResponse(array("status"=>array("message"=>strtoupper($verb)." method not allowed","code"=>403,"setHTTPStatus"=>true),"params"=>$params));
				}
			}
		}

		public function addComment($params) {
			if(!isset($params["comment"]) || !strlen($params["comment"])) {
				return ResponseManager::getResponse(array("status"=>array("message"=>"comment needs to be present","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
			}

			if(!isset($params["paraId"]) || !strlen($params["paraId"])) {
				return ResponseManager::getResponse(array("status"=>array("message"=>"paraId needs to be present","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
			}

			$db = new PDOWrapper();

			$checkParaId = "SELECT para FROM content WHERE para_id=?";
			$paraIdExists = $db->pdoQuery($checkParaId, array($params["paraId"]))->affectedRows() > 0 ? true : false;
			
			if(!$paraIdExists) {
				return ResponseManager::getResponse(array("status"=>array("message"=>"paraId invalid","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
			}

			if(!isset($params["postId"]) || !strlen($params["postId"])) {
				return ResponseManager::getResponse(array("status"=>array("message"=>"postId needs to be present","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
			}

			$checkPostId = "SELECT title FROM posts WHERE post_id=?";
			$postIdExists = $db->pdoQuery($checkPostId, array($params["postId"]))->affectedRows() > 0 ? true : false;
			
			if(!$postIdExists) {
				return ResponseManager::getResponse(array("status"=>array("message"=>"postId invalid","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
			}

			$commentInsert = "INSERT INTO comments (comment, para_id, post_id) VALUES (?,?,?)";

			$db->pdoQuery($commentInsert, array($params["comment"], $params["paraId"], $params["postId"]));

			return ResponseManager::getResponse(array("data"=>$data,"status"=>array("message"=>"comment inserted"),"rootParams"=>array("page"=>1),"params"=>$params));
		}

		public function deleteComment($params) {
			if(!isset($params["commentId"]) || !strlen($params["commentId"])) {
				return ResponseManager::getResponse(array("status"=>array("message"=>"commentId needs to be present","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
			}

			$db = new PDOWrapper();

			$deleteComment = "DELETE FROM comments WHERE comment_id=?";
			$isDeleted = $db->pdoQuery($deleteComment, array($params["commentId"]))->affectedRows() > 0 ? true : false;

			if(!$isDeleted) {
				return ResponseManager::getResponse(array("status"=>array("message"=>"commentId invalid","code"=>400,"setHTTPStatus"=>true),"params"=>$params));
			}

			return ResponseManager::getResponse(array("data"=>array("commentId"=>$commentId),"status"=>array("message"=>"post deleted"),"rootParams"=>array("page"=>1),"params"=>$params));

		}
	}

?>