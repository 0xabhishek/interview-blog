<?php

	class Users {

		public static function getUser($accessToken,$db=null) {
			$db = $db ? $db : new PDOWrapper();
			$query = "SELECT user_id userId, username, access_token accessToken FROM users WHERE access_token=?";
			$results = $db->pdoQuery($query,array($accessToken))->results();
			if(count($results) === 0) {
				return null;
			}
			return $results[0];
		}

	}

?>